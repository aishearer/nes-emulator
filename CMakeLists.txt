cmake_minimum_required (VERSION 2.6)
project (NES_Emulator)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

include_directories(include)
file(GLOB SOURCES "src/*.cpp")

add_executable(nes-emulator ${SOURCES})
