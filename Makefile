APPNAME := nes-emulator

CXX := g++
CXXFLAGS := -std=c++11 -Iinclude

TESTNAME := runUnitTests
TESTDIR := test
LIBGTEST := /usr/lib/libgtest_main.a /usr/lib/libgtest.a
LIBGMOCK := /usr/lib/gmock-all.o /usr/lib/libgmock.a

SRCFILES := $(shell find ./src -name "*.cpp")
OBJECTS  := $(patsubst %.cpp, %.o, $(SRCFILES))

all: $(APPNAME)

$(APPNAME): $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o $(APPNAME) $(OBJECTS)

test: $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o $(TESTNAME) $(TESTDIR)/*.cpp src/cpu.cpp src/ram.cpp $(LIBGTEST) $(LIBGMOCK) -lgmock -lpthread
	./$(TESTNAME)

depend: .depend

.depend: $(SRCFILES)
	rm -f ./.depend
	$(CXX) $(CXXFLAGS) -MM $^>>./.depend;

clean:
	rm -f $(OBJECTS)

dist-clean: clean
	rm -f *~ .depend

include .depend
