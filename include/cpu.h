#ifndef CPU_H
#define CPU_H

#include <cstdint>
#include <functional>
#include "ram.h"

class CPU {
public:
	CPU(Ram& ram);

	bool getCarryFlag();
	bool getZeroFlag();
	bool getInterruptFlag();
	bool getDecimalFlag();
	bool getBreakFlag();
	bool getOverflowFlag();
	bool getNegativeFlag();

	void step();

private:
	uint8_t _x;
	uint8_t _y;
	uint8_t _a;
	uint16_t _pc;
	uint8_t _sp;

	bool _carryFlag;
	bool _zeroFlag;
	bool _interruptFlag;
	bool _decimalFlag;
	bool _breakFlag;
	bool _overflowFlag;
	bool _negativeFlag;

	Ram _ram;

	std::function<void()> operations[0xFF];

	void setNeg(uint8_t operand);
	void setZero(uint8_t operand);

	void setCarryFlag(bool flag);
	void setZeroFlag(bool flag);
	void setInterruptFlag(bool flag);
	void setDecimalFlag(bool flag);
	void setBreakFlag(bool flag);
	void setOverflowFlag(bool flag);
	void setNegativeFlag(bool flag);

	uint8_t readNext8();
	uint16_t readNext16();
};

#endif
