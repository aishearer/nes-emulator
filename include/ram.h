#ifndef RAM_H
#define RAM_H

#include <cstdint>


class Ram {
public:
	Ram();
	uint8_t operator[] (int i) const;
	uint8_t& operator[] (int i);

	uint8_t readNext8();
	uint16_t readNext16();

private:
	uint8_t _memory[0xFFFF];


};

#endif
