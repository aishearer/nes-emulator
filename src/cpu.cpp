#include "cpu.h"
#include <iostream>

CPU::CPU(Ram& ram) {

	// registers
	_x = 0;
	_y = 0;
	_a = 0;
	_pc = 0; // TODO set to $FFFC + $FFFD * 256
	_sp = 0xFF;

	_carryFlag = false;
	_zeroFlag = false;
	_interruptFlag = false;
	_decimalFlag = false;
	_breakFlag = false;
	_overflowFlag = false;
	_negativeFlag = false;

	_ram = ram;

	// Load opcode lambda functions

	// Load
	operations[0xA9] = [this]() { uint8_t o1 = readNext8(); setNeg(o1); setZero(o1); _a = o1; };
	operations[0xA1] = [this]() { uint8_t o1 = readNext8(); setNeg(o1); setZero(o1); _x = o1; };
	operations[0xA0] = [this]() { uint8_t o1 = readNext8(); setNeg(o1); setZero(o1); _y = o1; };

	// Transfer
	operations[0xAA] = [this]() { setNeg(_a); setZero(_a); _x = _a; };
	operations[0xA8] = [this]() { setNeg(_a); setZero(_a); _y = _a; };
	operations[0xBA] = [this]() { setNeg(_sp); setZero(_sp); _x = _sp; };
	operations[0x8A] = [this]() { setNeg(_x); setZero(_x); _a = _x; };
	operations[0x9A] = [this]() { setNeg(_x); setZero(_x); _sp = _x; };
	operations[0x98] = [this]() { setNeg(_y); setZero(_y); _a = _y; };

	// Clear
	operations[0x18] = [this]() { setCarryFlag(false); };
	operations[0xD8] = [this]() { setDecimalFlag(false); };
	operations[0x58] = [this]() { setInterruptFlag(false); };
	operations[0xB8] = [this]() { setOverflowFlag(false); };

}

/*
 * Execute one cycle of the cpu
 */
void CPU::step() {
	operations[0xA9]();
	_pc++;
	//std::cout << unsigned(this->_a) << " " << this->_zeroFlag << " " << this->_negativeFlag;
}

// Flag getters
bool CPU::getCarryFlag()		{ return _carryFlag;		}
bool CPU::getZeroFlag()			{ return _zeroFlag;			}
bool CPU::getInterruptFlag()	{ return _interruptFlag;	}
bool CPU::getDecimalFlag()		{ return _decimalFlag;		}
bool CPU::getBreakFlag()		{ return _breakFlag;		}
bool CPU::getOverflowFlag()		{ return _overflowFlag;		}
bool CPU::getNegativeFlag()		{ return _negativeFlag;		}
// Flag setters
void CPU::setCarryFlag(bool flag)		{ _carryFlag = flag;		}
void CPU::setZeroFlag(bool flag)		{ _zeroFlag = flag;			}
void CPU::setInterruptFlag(bool flag)	{ _interruptFlag = flag;	}
void CPU::setDecimalFlag(bool flag)		{ _decimalFlag = flag;		}
void CPU::setBreakFlag(bool flag)		{ _breakFlag = flag;		}
void CPU::setOverflowFlag(bool flag)	{ _overflowFlag = flag;		}
void CPU::setNegativeFlag(bool flag)	{ _negativeFlag = flag;		}

/*
 * Set the negative flag based on the value of operand
 */
void CPU::setNeg(uint8_t operand) {
	// if the 7th bit of operand is set, enable the negative flag
	setNegativeFlag(((operand >> 7) & 1) == 1);
}

/*
 * Set the zero flag if operand is zero
 */
void CPU::setZero(uint8_t operand) {
	setZeroFlag(operand == 0);
}

uint8_t CPU::readNext8() {
	_pc++;
	return _ram.readNext8();
}

uint16_t CPU::readNext16() {
	_pc += 2;
	return _ram.readNext16();
}
