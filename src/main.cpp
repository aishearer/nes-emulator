#include <iostream>
#include "cpu.h"

int main(int argc, char** argv) {
	std::cout << "Welcome to NES emulator" << std::endl;
	Ram r;
	CPU cpu(r);
	cpu.step();

	return 0;
}
