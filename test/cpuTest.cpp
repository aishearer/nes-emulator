#include "cpu.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

TEST(CPUTest, CarryFlagStartsFalse) {
	Ram r;
	CPU cpu(r);
	EXPECT_FALSE(cpu.getCarryFlag());
}

TEST(CPUTest, ZeroFlagStartsFalse) {
	Ram r;
	CPU cpu(r);
	EXPECT_FALSE(cpu.getZeroFlag());
}

TEST(CPUTest, InterruptFlagStartsFalse) {
	Ram r;
	CPU cpu(r);
	EXPECT_FALSE(cpu.getInterruptFlag());
}

TEST(CPUTest, DecimalFlagStartsFalse) {
	Ram r;
	CPU cpu(r);
	EXPECT_FALSE(cpu.getDecimalFlag());
}

TEST(CPUTest, BreakFlagStartsFalse) {
	Ram r;
	CPU cpu(r);
	EXPECT_FALSE(cpu.getBreakFlag());
}

TEST(CPUTest, OverflowFlagStartsFalse) {
	Ram r;
	CPU cpu(r);
	EXPECT_FALSE(cpu.getOverflowFlag());
}

TEST(CPUTest, NegativeFlagStartsFalse) {
	Ram r;
	CPU cpu(r);
	EXPECT_FALSE(cpu.getNegativeFlag());
}

int main(int argc, char **argv)
{
	::testing::InitGoogleMock(&argc, argv);
	return RUN_ALL_TESTS();
}
